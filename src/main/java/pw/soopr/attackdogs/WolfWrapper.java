package pw.soopr.attackdogs;

import net.minecraft.server.v1_8_R3.AttributeInstance;
import net.minecraft.server.v1_8_R3.AttributeModifier;
import net.minecraft.server.v1_8_R3.EntityInsentient;
import net.minecraft.server.v1_8_R3.GenericAttributes;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftLivingEntity;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftWolf;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import pw.soopr.attackdogs.gui.GUIHandle;
import pw.soopr.attackdogs.gui.UpgradeContainer;

import java.util.UUID;

/**
 * Created by Aaron.
 */
public class WolfWrapper {

    private double speed;
    private double damage;
    private double regen;
    int sl;
    int dl;
    int rl;
    private Wolf wolf;
    private Player p;
    EntityInsentient nmsEntity;

    public WolfWrapper(Player p, double speed, double damage, double regen,int sl, int dl, int rl){
        this.speed = speed;
        this.damage = damage;
        this.regen = regen;
        this.sl = sl;
        this.dl = dl;
        this.rl = rl;
        this.p = p;
    }

    public void spawn(Location loc){
        this.wolf = (Wolf) loc.getWorld().spawnEntity(loc, EntityType.WOLF);
        nmsEntity = (EntityInsentient) ((CraftLivingEntity) wolf).getHandle();

        //Setting Max Health
        AttributeInstance attributes = nmsEntity.getAttributeInstance(GenericAttributes.maxHealth);
        UUID hUUID = UUID.randomUUID();
        AttributeModifier modifier = new AttributeModifier(hUUID,"AttackDog health",52.0,0);
        attributes.b(modifier);
        attributes.a(modifier);

        //Setting speed
        AttributeInstance sAttributes = nmsEntity.getAttributeInstance(GenericAttributes.MOVEMENT_SPEED);
        UUID sUUID = UUID.randomUUID();
        AttributeModifier sModifier = new AttributeModifier(sUUID,"AttackDog speed",speed/10,1);
        sAttributes.b(sModifier);
        sAttributes.a(sModifier);

        //Setting Damage
        AttributeInstance dAttributes = nmsEntity.getAttributeInstance(GenericAttributes.ATTACK_DAMAGE);
        UUID dUUID = UUID.randomUUID();
        AttributeModifier dModifier = new AttributeModifier(dUUID,"AttackDog damage",damage,0);
        dAttributes.b(dModifier);
        dAttributes.a(dModifier);

        nmsEntity.setHealth(nmsEntity.getMaxHealth());
        ((CraftWolf) wolf).getHandle().setOwnerUUID(p.getUniqueId().toString());
        ((CraftWolf) wolf).getHandle().setTamed(true);
        ((CraftWolf) wolf).getHandle().setSitting(false);

    }

    public void upgrade(final Player p, GUIHandle.UpgradeType type){
        UpgradeContainer container = AttackDogs.getI().getUpgradeContainer(type);
        if(type == GUIHandle.UpgradeType.SPEED){
            final double newstat = container.getNextStat(p);
            final WolfWrapper wrapper = AttackDogs.getI().getWolfMap().get(p.getUniqueId());
            wrapper.setSpeed(newstat);
            wrapper.setSl(wrapper.getSl() + 1);
            Bukkit.getScheduler().runTaskAsynchronously(AttackDogs.getI(), new Runnable() {
                @Override
                public void run() {
                    AttackDogs.getI().getDb().updateStat(p.getUniqueId(),"speed",newstat);
                    AttackDogs.getI().getDb().updateStat(p.getUniqueId(),"sl",wrapper.getSl());
                }
            });
        }
        if(type == GUIHandle.UpgradeType.DAMAGE){
            final double newstat = container.getNextStat(p);
            final WolfWrapper wrapper = AttackDogs.getI().getWolfMap().get(p.getUniqueId());
            wrapper.setDamage(newstat);
            wrapper.setDl(wrapper.getDl() + 1);
            Bukkit.getScheduler().runTaskAsynchronously(AttackDogs.getI(), new Runnable() {
                @Override
                public void run() {
                    AttackDogs.getI().getDb().updateStat(p.getUniqueId(), "damage", newstat);
                    AttackDogs.getI().getDb().updateStat(p.getUniqueId(),"dl",wrapper.getDl());
                }
            });
        }
        if(type == GUIHandle.UpgradeType.REGEN){
            final double newstat = container.getNextStat(p);
            final WolfWrapper wrapper = AttackDogs.getI().getWolfMap().get(p.getUniqueId());
            wrapper.setRegen(newstat);
            wrapper.setRl(wrapper.getRl() + 1);
            Bukkit.getScheduler().runTaskAsynchronously(AttackDogs.getI(), new Runnable() {
                @Override
                public void run() {
                    AttackDogs.getI().getDb().updateStat(p.getUniqueId(), "regen", newstat);
                    AttackDogs.getI().getDb().updateStat(p.getUniqueId(),"rl",wrapper.getRl());
                }
            });
        }
        if(!isAlive()) return;

        //Setting Max Health
        AttributeInstance attributes = nmsEntity.getAttributeInstance(GenericAttributes.maxHealth);
        UUID hUUID = UUID.randomUUID();
        AttributeModifier modifier = new AttributeModifier(hUUID,"AttackDog health",48.0,0);
        attributes.b(modifier);
        attributes.a(modifier);

        //Setting speed
        if(type == GUIHandle.UpgradeType.SPEED) {
            AttributeInstance sAttributes = nmsEntity.getAttributeInstance(GenericAttributes.MOVEMENT_SPEED);
            UUID sUUID = UUID.randomUUID();
            AttributeModifier sModifier = new AttributeModifier(sUUID, "AttackDog speed", speed, 0);
            sAttributes.b(sModifier);
            sAttributes.a(sModifier);
        }

        //Setting Damage
        if(type == GUIHandle.UpgradeType.DAMAGE) {
            AttributeInstance dAttributes = nmsEntity.getAttributeInstance(GenericAttributes.ATTACK_DAMAGE);
            UUID dUUID = UUID.randomUUID();
            AttributeModifier dModifier = new AttributeModifier(dUUID, "AttackDog damage", damage, 0);
            dAttributes.b(dModifier);
            dAttributes.a(dModifier);
        }

        Bukkit.broadcastMessage(AttackDogs.getI().getWolfMap().get(p.getUniqueId()).toString());
    }

    @Override
    public String toString(){
        return "SPEED: " + getActualSpeed() + "\nDMG: " + nmsEntity.getAttributeInstance(GenericAttributes.ATTACK_DAMAGE).getValue() + "\nREGEN: " + regen;
    }

    public double getSpeed() {
        return speed;
    }

    public double getActualSpeed() {
        return nmsEntity == null ? getSpeed() : nmsEntity.getAttributeInstance(GenericAttributes.MOVEMENT_SPEED).getValue();
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getDamage() {
        return damage;
    }

    public void setDamage(double damage) {
        this.damage = damage;
    }

    public double getRegen() {
        return regen;
    }

    public void setRegen(double regen) {
        this.regen = regen;
    }

    public Wolf getWolf() {
        return wolf;
    }

    public void setWolf(Wolf wolf) {
        this.wolf = wolf;
    }

    public boolean isAlive(){
        return wolf != null;
    }

    public void die(){
        wolf = null;
    }

    public int getRl() {
        return rl;
    }

    public void setRl(int rl) {
        this.rl = rl;
    }

    public int getSl() {
        return sl;
    }

    public void setSl(int sl) {
        this.sl = sl;
    }

    public int getDl() {
        return dl;
    }

    public void setDl(int dl) {
        this.dl = dl;
    }
}
