package pw.soopr.attackdogs;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.ChatColor;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import pw.soopr.attackdogs.db.SQLUtils;
import pw.soopr.attackdogs.file.YAMLFile;
import pw.soopr.attackdogs.gui.GUIHandle;
import pw.soopr.attackdogs.gui.UpgradeContainer;
import pw.soopr.attackdogs.listener.ConnectionListener;
import pw.soopr.attackdogs.listener.GUIListener;
import pw.soopr.attackdogs.listener.UpgradeListener;
import pw.soopr.attackdogs.listener.WolfListener;

import java.util.*;

/**
 * Created by Aaron.
 */
public class AttackDogs extends JavaPlugin{

    private static AttackDogs i;

    private Map<UUID, WolfWrapper> wolfWrapperMap;
    private Map<UUID, GUIHandle> guis;
    private Map<UUID, Long> cooldowns;

    private SQLUtils db;
    private YAMLFile dbfile;
    private YAMLFile formatfile;
    private UpgradeContainer[] containers;
    private Economy economy;

    @Override
    public void onEnable(){
        i = this;
        wolfWrapperMap = new HashMap<>();
        guis = new HashMap<>();
        cooldowns = new HashMap<>();

        dbfile = new YAMLFile(this,"db.yml");
        dbfile.reloadConfig();

        formatfile = new YAMLFile(this,"formats.yml");
        formatfile.reloadConfig();

        db = new SQLUtils(getRawString(dbfile,"host"),getRawInt(dbfile,"port"),getRawString(dbfile,"db"),getRawString(dbfile,"user"),getRawString(dbfile,"pass"));
        db.initializeTable();
        initializeContainerArray();

        setupEconomy();

        getServer().getPluginManager().registerEvents(new ConnectionListener(),this);
        getServer().getPluginManager().registerEvents(new GUIListener(), this);
        getServer().getPluginManager().registerEvents(new UpgradeListener(), this);
        getServer().getPluginManager().registerEvents(new WolfListener(), this);

        getServer().getScheduler().scheduleSyncRepeatingTask(this, new RegenTask(), 10, 50);

        getCommand("attackdog").setExecutor(new AttackDogCommand());
    }

    public static AttackDogs getI(){
        return i;
    }

    public Map<UUID, WolfWrapper> getWolfMap() {
        return wolfWrapperMap;
    }

    public void runAsync(Runnable run){
        getServer().getScheduler().runTaskAsynchronously(this,run);
    }
    public void runSync(Runnable run){
        getServer().getScheduler().runTask(this, run);
    }

    public SQLUtils getDb() {
        return db;
    }

    public String getRawString(YAMLFile file, String path){
         return file.getConfig().getString(path);
    }
    public int getRawInt(YAMLFile file, String path){
        return file.getConfig().getInt(path);
    }


    public String getString(YAMLFile file, String path){
        return ChatColor.translateAlternateColorCodes('&', file.getConfig().getString(path));
    }

    public UpgradeContainer getUpgradeContainer(GUIHandle.UpgradeType type) {
        if(type == GUIHandle.UpgradeType.SPEED) return containers[0];
        if(type == GUIHandle.UpgradeType.DAMAGE) return containers[1];
        return containers[2];
    }

    public String getF(String path){
        return ChatColor.translateAlternateColorCodes('&',formatfile.getConfig().getString(path));
    }

    public void initializeContainerArray(){
        containers = new UpgradeContainer[3];
        {
            TreeMap<Integer, Double> speed = new TreeMap();
            speed.put(5000, 0.0);
            speed.put(10000, .18);
            speed.put(25000, .11);
            speed.put(50000, .11);
            speed.put(65000, .11);
            containers[0] = new UpgradeContainer(speed, GUIHandle.UpgradeType.SPEED);
        }

        {
            TreeMap<Integer, Double> speed = new TreeMap();
            speed.put(10000, 8.0);
            speed.put(20000, 3.0);
            speed.put(50000, 3.0);
            speed.put(65000, 3.0);
            speed.put(100000, 2.0);
            containers[1] = new UpgradeContainer(speed, GUIHandle.UpgradeType.DAMAGE);
        }

        {
            TreeMap<Integer, Double> speed = new TreeMap();
            speed.put(7500, 1.0);
            speed.put(15000, 2.5);
            speed.put(20000, 4.0);
            speed.put(35000, 6.0);
            speed.put(50000, 8.0);
            containers[2] = new UpgradeContainer(speed, GUIHandle.UpgradeType.REGEN);
        }
    }

    private boolean setupEconomy()
    {
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }

        return (economy != null);
    }

    public Economy getEconomy() {
        return economy;
    }

    public Map<UUID, GUIHandle> getGUIMap() {
        return guis;
    }

    public UpgradeContainer[] getContainers() {
        return containers;
    }

    public Map<UUID, Long> getCooldowns() {
        return cooldowns;
    }

    public YAMLFile getDbfile() {
        return dbfile;
    }

    public YAMLFile getFormatfile() {
        return formatfile;
    }
}
