package pw.soopr.attackdogs;

import org.bukkit.scheduler.BukkitRunnable;

import java.util.Map;
import java.util.UUID;

/**
 * Created by Aaron.
 */
public class RegenTask extends BukkitRunnable{
    @Override
    public void run() {
        for(Map.Entry<UUID,WolfWrapper> entry : AttackDogs.getI().getWolfMap().entrySet()){
            if(entry.getValue().isAlive()){
                entry.getValue().getWolf().setHealth(entry.getValue().getWolf().getHealth() + entry.getValue().getRegen()/2 > entry.getValue().getWolf().getMaxHealth()
                        ? entry.getValue().getWolf().getMaxHealth() : entry.getValue().getWolf().getHealth() + entry.getValue().getRegen()/2);
            }
        }
    }
}
