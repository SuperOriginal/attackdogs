package pw.soopr.attackdogs.db;

import lombok.SneakyThrows;
import org.bukkit.Bukkit;
import pw.soopr.attackdogs.AttackDogs;
import pw.soopr.attackdogs.WolfWrapper;

import java.sql.*;
import java.util.UUID;

/**
 * Created by Aaron.
 */
public class SQLUtils {

    private Connection conn;

    public SQLUtils(String host, int port, String db, String user, String pass){
        try{
            conn = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + db, user, pass);
        }catch (SQLException e){
            e.printStackTrace();
            Bukkit.getLogger().severe("ERROR CONNECTING TO DATABASE! MAKE SURE db.yml IS CONFIGURED!");
        }
    }

    @SneakyThrows
    public void initializeTable(){
        PreparedStatement statement = conn.prepareStatement("CREATE TABLE IF NOT EXISTS wolf_data(" +
                "uuid VARCHAR(64) NOT NULL," +
                "speed DOUBLE NOT NULL," +
                "damage DOUBLE NOT NULL," +
                "regen DOUBLE NOT NULL," +
                "sl INT NOT NULL," +
                "dl INT NOT NULL," +
                "rl INT NOT NULL);");
        statement.execute();
        statement.close();
    }

    @SneakyThrows
    public boolean containedInDB(UUID uuid){
        PreparedStatement statement = conn.prepareStatement("SELECT * FROM wolf_data WHERE uuid = ?");
        statement.setString(1,uuid.toString());

        ResultSet set = statement.executeQuery();
        return set.next();
    }

    @SneakyThrows
    public void generateDefaults(UUID uuid){
        PreparedStatement statement = conn.prepareStatement("INSERT INTO wolf_data (uuid,speed,damage,regen,sl,dl,rl) VALUES (?,1,8,1,1,1,1)");
        statement.setString(1,uuid.toString());

        statement.execute();
    }

    @SneakyThrows
    public void updateStat(UUID uuid, String stat, double value){
        PreparedStatement statement = conn.prepareStatement("UPDATE wolf_data SET " + stat + " = ? WHERE uuid = ?");
        statement.setDouble(1,value);
        statement.setString(2,uuid.toString());
        statement.executeUpdate();
    }

    @SneakyThrows
    public WolfWrapper decodeWrapper(UUID uuid){
        PreparedStatement statement = conn.prepareStatement("SELECT * FROM wolf_data WHERE uuid = ?");
        statement.setString(1,uuid.toString());

        ResultSet set = statement.executeQuery();
        if(set.next()){
            return new WolfWrapper(Bukkit.getPlayer(uuid),set.getDouble("speed"), set.getDouble("damage"), set.getDouble("regen"), set.getInt("sl"), set.getInt("dl"),set.getInt("rl"));
        }
        return null;
    }

}
