package pw.soopr.attackdogs;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Aaron.
 */
public class AttackDogCommand implements CommandExecutor{

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args){
        if(cmd.getName().equalsIgnoreCase("attackdog")){
            if(args.length > 0){
                if(args[0].equalsIgnoreCase("shutdown")){
                    if(sender.hasPermission("gtamc.attackdog.shutdown")){
                        sender.sendMessage(ChatColor.GREEN + "Successfully disabled AttackDogs.");
                        Bukkit.getServer().getPluginManager().disablePlugin(AttackDogs.getI());
                        return true;
                    }
                }
                if(args[0].equalsIgnoreCase("reload")){
                    if(sender.hasPermission("gtamc.attackdog.reload")){
                        AttackDogs.getI().getFormatfile().reloadConfig();
                        AttackDogs.getI().getDbfile().reloadConfig();
                        sender.sendMessage(ChatColor.GREEN + "Successfully reloaded AttackDogs.");
                        return true;
                    }
                }
            }
            if(!(sender instanceof Player)){
                sender.sendMessage(AttackDogs.getI().getF("error.noconsole"));
                return true;
            }

            Player p = (Player) sender;
            if(!p.hasPermission("gtamc.attackdog.user")){
                p.sendMessage(AttackDogs.getI().getF("error.noaccess"));
                return true;
            }

            if(AttackDogs.getI().getCooldowns().containsKey(p.getUniqueId()) && !p.hasPermission("gtamc.attackdog.bypass")){
                if(System.currentTimeMillis() >= AttackDogs.getI().getCooldowns().get(p.getUniqueId()) + 600000){
                    AttackDogs.getI().getCooldowns().remove(p.getUniqueId());
                }else{
                    p.sendMessage(AttackDogs.getI().getF("spawn.cooldown"));
                    return true;
                }
            }
            AttackDogs.getI().getWolfMap().get(p.getUniqueId()).spawn(p.getLocation());
            AttackDogs.getI().getCooldowns().put(p.getUniqueId(), System.currentTimeMillis());
            p.sendMessage(AttackDogs.getI().getF("spawn.success"));
            Bukkit.broadcastMessage(AttackDogs.getI().getWolfMap().get(p.getUniqueId()).toString());
        }
        return true;
    }
}
