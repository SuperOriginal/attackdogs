package pw.soopr.attackdogs.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import pw.soopr.attackdogs.AttackDogs;
import pw.soopr.attackdogs.gui.GUIHandle;

/**
 * Created by Aaron.
 */
public class UpgradeListener implements Listener{

    @EventHandler
    public void onUpgrade(PlayerInteractEntityEvent e){
        if(AttackDogs.getI().getWolfMap().get(e.getPlayer().getUniqueId()).isAlive() && e.getRightClicked().getUniqueId() == AttackDogs.getI().getWolfMap().get(e.getPlayer().getUniqueId()).getWolf().getUniqueId() && e.getPlayer().isSneaking()){
            e.setCancelled(true);
            GUIHandle handle = new GUIHandle(e.getPlayer());
            AttackDogs.getI().getGUIMap().put(e.getPlayer().getUniqueId(),handle);
            handle.openSelector();
        }
    }
}
