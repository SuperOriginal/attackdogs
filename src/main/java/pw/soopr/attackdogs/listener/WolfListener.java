package pw.soopr.attackdogs.listener;

import net.minecraft.server.v1_8_R3.EntityLiving;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftWolf;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.*;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import pw.soopr.attackdogs.AttackDogs;
import pw.soopr.attackdogs.WolfWrapper;

import java.util.Map;
import java.util.UUID;

/**
 * Created by Aaron.
 */
public class WolfListener implements Listener{
    @EventHandler
    public void onDie(EntityDeathEvent e){
        for(Map.Entry<UUID,WolfWrapper> entry : AttackDogs.getI().getWolfMap().entrySet()){
            if(entry.getValue().isAlive()){
                if(entry.getValue().getWolf().getUniqueId() == e.getEntity().getUniqueId()){
                    entry.getValue().die();
                }
            }
        }
    }

    /*@EventHandler
    public void onPDead(PlayerDeathEvent e){
        if(e.getEntity().getLastDamageCause() instanceof EntityDamageByEntityEvent){
            EntityDamageByEntityEvent ev = (EntityDamageByEntityEvent) e.getEntity().getLastDamageCause();
            if(ev.getDamager() instanceof Wolf){
                for(Map.Entry<UUID,WolfWrapper> entry : AttackDogs.getI().getWolfMap().entrySet()){
                    if(entry.getValue().isAlive() && entry.getValue().getWolf().getUniqueId() == ev.getDamager().getUniqueId()){
                        Wolf wickedWolf = entry.getValue().getWolf();
                        ((CraftWolf)wickedWolf).getHandle().setAngry(false);
                        ((CraftWolf)wickedWolf).getHandle().setGoalTarget(((CraftPlayer)Bukkit.getPlayer(entry.getKey())).getHandle(), EntityTargetEvent.TargetReason.TARGET_DIED, true);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent e){
        if(e.getEntity() instanceof Wolf && e.getDamager() instanceof Player){
            Player damager = (Player) e.getDamager();
            if(!AttackDogs.getI().getWolfMap().get(damager.getUniqueId()).isAlive() || AttackDogs.getI().getWolfMap().get(damager.getUniqueId()).getWolf().getUniqueId() != e.getEntity().getUniqueId()){
                Wolf saltyWolf = (Wolf) e.getEntity();
                ((CraftWolf)saltyWolf).getHandle().setAngry(true);
                ((CraftWolf)saltyWolf).getHandle().setGoalTarget(((CraftPlayer)e.getDamager()).getHandle(), EntityTargetEvent.TargetReason.TARGET_ATTACKED_ENTITY,true);
            }
        }

        if(e.getDamager() instanceof Player && e.getEntity() instanceof Player){
            if(AttackDogs.getI().getWolfMap().get(e.getEntity().getUniqueId()).isAlive()){
                Wolf wolf = AttackDogs.getI().getWolfMap().get(e.getEntity().getUniqueId()).getWolf();
                ((CraftWolf)wolf).getHandle().setAngry(true);
                ((CraftWolf)wolf).getHandle().setGoalTarget(((CraftPlayer)e.getDamager()).getHandle(), EntityTargetEvent.TargetReason.TARGET_ATTACKED_OWNER,true);
            }
        }
    }*/

    @EventHandler
    public void onRegen(EntityRegainHealthEvent e){
        if(e.getEntity() instanceof Wolf){
            for(Map.Entry<UUID,WolfWrapper> entry : AttackDogs.getI().getWolfMap().entrySet()){
                if(entry.getValue().getWolf().getUniqueId() == e.getEntity().getUniqueId()){
                    e.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractAtEntityEvent e){
        if(e.getRightClicked() instanceof Wolf){
            e.setCancelled(true);
        }
    }
}
