package pw.soopr.attackdogs.listener;

import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import pw.soopr.attackdogs.AttackDogs;
import pw.soopr.attackdogs.gui.GUIHandle;
import pw.soopr.attackdogs.gui.UpgradeContainer;

/**
 * Created by Aaron.
 */
public class GUIListener implements Listener{
    @EventHandler
    public void onSelect(InventoryClickEvent e){
        if(AttackDogs.getI().getGUIMap().containsKey(e.getWhoClicked().getUniqueId())){
                if(e.getCurrentItem() != null){
                    e.setCancelled(true);
                    GUIHandle handle = AttackDogs.getI().getGUIMap().get(e.getWhoClicked().getUniqueId());
                    if(handle.getType() != null) return;
                    if(e.getCurrentItem().isSimilar(handle.getSpeed())){
                        handle.setType(GUIHandle.UpgradeType.SPEED);
                        handle.openUpgrade(GUIHandle.UpgradeType.SPEED);
                    }else if(e.getCurrentItem().isSimilar(handle.getDamage())){
                        handle.setType(GUIHandle.UpgradeType.DAMAGE);
                        handle.openUpgrade(GUIHandle.UpgradeType.DAMAGE);
                    }else if(e.getCurrentItem().isSimilar(handle.getRegen())){
                        handle.setType(GUIHandle.UpgradeType.REGEN);
                        handle.openUpgrade(GUIHandle.UpgradeType.REGEN);
                    }
                }
            }
    }

    @EventHandler
    public void onUpgrade(InventoryClickEvent e){
        if(AttackDogs.getI().getGUIMap().containsKey(e.getWhoClicked().getUniqueId())) {
                if(e.getCurrentItem() != null){
                    e.setCancelled(true);
                    if(AttackDogs.getI().getGUIMap().get(e.getWhoClicked().getUniqueId()).getType() == null) return;
                    UpgradeContainer container = AttackDogs.getI().getUpgradeContainer(AttackDogs.getI().getGUIMap().get(e.getWhoClicked().getUniqueId()).getType());
                        if(e.getCurrentItem().isSimilar(container.getNext())){
                            EconomyResponse response = AttackDogs.getI().getEconomy().withdrawPlayer((Player)e.getWhoClicked(),container.getCost((Player) e.getWhoClicked(), AttackDogs.getI().getGUIMap().get(e.getWhoClicked().getUniqueId()).getType()));
                            if(response.transactionSuccess()) {
                                AttackDogs.getI().getWolfMap().get(e.getWhoClicked().getUniqueId()).upgrade((Player) e.getWhoClicked(), container.getType());
                                ((Player) e.getWhoClicked()).closeInventory();
                                AttackDogs.getI().getGUIMap().remove(e.getWhoClicked().getUniqueId());
                                ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.LEVEL_UP, 1, 1);
                            }else{
                                AttackDogs.getI().getGUIMap().remove(e.getWhoClicked().getUniqueId());
                                ((Player) e.getWhoClicked()).closeInventory();
                                e.getWhoClicked().sendMessage(AttackDogs.getI().getF("gui.error.nomoney"));
                            }
                        }
                    }
                }
        }

    @EventHandler
    public void onClose(InventoryCloseEvent e){
        Bukkit.getScheduler().scheduleSyncDelayedTask(AttackDogs.getI(), new Runnable() {
            @Override
            public void run() {
                if(e.getPlayer().getOpenInventory() == null && AttackDogs.getI().getGUIMap().containsKey(e.getPlayer().getUniqueId())){
                    AttackDogs.getI().getGUIMap().remove(e.getPlayer().getUniqueId());
                }
            }
        },3);

    }

}
