package pw.soopr.attackdogs.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import pw.soopr.attackdogs.AttackDogs;
import pw.soopr.attackdogs.WolfWrapper;

/**
 * Created by Aaron.
 */
public class ConnectionListener implements Listener{

    @EventHandler
    public void onJoin(PlayerJoinEvent e){
            AttackDogs.getI().runAsync(new Runnable() {
                @Override
                public void run() {
                    if(AttackDogs.getI().getDb().containedInDB(e.getPlayer().getUniqueId())){
                        AttackDogs.getI().getWolfMap().put(e.getPlayer().getUniqueId(),AttackDogs.getI().getDb().decodeWrapper(e.getPlayer().getUniqueId()));
                    }else{
                        AttackDogs.getI().getDb().generateDefaults(e.getPlayer().getUniqueId());
                        AttackDogs.getI().getWolfMap().put(e.getPlayer().getUniqueId(),new WolfWrapper(e.getPlayer(),1,8,1,1,1,1));
                    }
                }
            });
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e){
        if(AttackDogs.getI().getWolfMap().get(e.getPlayer().getUniqueId()).isAlive()){
            AttackDogs.getI().getWolfMap().get(e.getPlayer().getUniqueId()).getWolf().remove();
        }
        AttackDogs.getI().getWolfMap().remove(e.getPlayer().getUniqueId());
    }

    @EventHandler
    public void onLeave(PlayerKickEvent e){
        if(AttackDogs.getI().getWolfMap().get(e.getPlayer().getUniqueId()).isAlive()){
            AttackDogs.getI().getWolfMap().get(e.getPlayer().getUniqueId()).getWolf().remove();
        }
        AttackDogs.getI().getWolfMap().remove(e.getPlayer().getUniqueId());
    }
}
