package pw.soopr.attackdogs.gui;

import org.apache.commons.lang.WordUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import pw.soopr.attackdogs.AttackDogs;

/**
 * Created by Aaron.
 */
public class GUIHandle {
    private Player p;
    private ItemStack speed;
    private ItemStack damage;
    private ItemStack regen;
    private UpgradeType type;

    public GUIHandle(Player p){
        this.p = p;
    }

    public void openSelector(){
        Inventory selector = Bukkit.createInventory(null,9, AttackDogs.getI().getF("gui.selector.upgradeselectorname"));

        {ItemStack speed = new ItemStack(Material.SUGAR);

        ItemMeta meta = speed.getItemMeta();
        meta.setDisplayName(AttackDogs.getI().getF("gui.selector.sugar.displayName"));
        //meta.setLore(AttackDogs.getI().getFList("gui.selector.sugar.lore"));
        speed.setItemMeta(meta);
            selector.setItem(0,speed);
            this.speed = speed;
        }

        {ItemStack speed = new ItemStack(Material.DIAMOND_SWORD);

            ItemMeta meta = speed.getItemMeta();
            meta.setDisplayName(AttackDogs.getI().getF("gui.selector.sword.displayName"));
            //meta.setLore(AttackDogs.getI().getFList("gui.selector.sword.lore"));
            speed.setItemMeta(meta);
            selector.setItem(4,speed);
            damage = speed;
        }

        {ItemStack speed = new ItemStack(Material.REDSTONE);

            ItemMeta meta = speed.getItemMeta();
            meta.setDisplayName(AttackDogs.getI().getF("gui.selector.redstone.displayName"));
            //meta.setLore(AttackDogs.getI().getFList("gui.selector.redstone.lore"));
            speed.setItemMeta(meta);
            selector.setItem(8,speed);
            regen = speed;
        }

        p.openInventory(selector);

    }

    public void openUpgrade(UpgradeType type){
        Inventory inv = Bukkit.createInventory(null,9,AttackDogs.getI().getF("gui.upgrade.menuname").replace("{type}", WordUtils.capitalize(type.toString().toLowerCase())));
        AttackDogs.getI().getUpgradeContainer(type).populateInventoryForPlayer(p,inv);
        p.openInventory(inv);
    }

    public ItemStack getSpeed() {
        return speed;
    }

    public ItemStack getDamage() {
        return damage;
    }

    public ItemStack getRegen() {
        return regen;
    }

    public enum UpgradeType{
        SPEED,
        DAMAGE,
        REGEN
    }

    public UpgradeType getType() {
        return type;
    }

    public void setType(UpgradeType type) {
        this.type = type;
    }
}
