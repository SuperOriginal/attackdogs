package pw.soopr.attackdogs.gui;

import org.apache.commons.lang.WordUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import pw.soopr.attackdogs.AttackDogs;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Aaron.
 */
public class UpgradeContainer {
    private Map<Integer,Double> costStatMap;
    private GUIHandle.UpgradeType type;
    private ItemStack unlocked;
    private ItemStack next;
    private ItemStack locked;

    public UpgradeContainer(Map<Integer,Double> map, GUIHandle.UpgradeType type){
        this.costStatMap = map;
        this.type = type;

        unlocked = new ItemStack(Material.WOOL,1,(short)5);

        next = new ItemStack(Material.WOOL,1,(short)14);

        locked = new ItemStack(Material.WOOL,1,(short)14);
        ItemMeta lMeta = locked.getItemMeta();
        ArrayList<String> ltest = new ArrayList<>();
        ltest.add(ChatColor.RED + "Locked");
        ltest.add(ChatColor.RED + "You must unlock all previous levels first.");
        lMeta.setDisplayName(AttackDogs.getI().getF("gui.upgrade.locked.displayName"));
        lMeta.setLore(ltest);
        locked.setItemMeta(lMeta);
    }

    public void populateInventoryForPlayer(Player p, Inventory populate){
        int i = getCurrentLevel(p);
        int c = 1;
        int slot = 0;
        double prev = 0;
        for(Map.Entry<Integer,Double> entry : costStatMap.entrySet()){
            if(c <= i){
                ItemMeta meta = unlocked.getItemMeta();
                meta.setDisplayName(AttackDogs.getI().getF("gui.upgrade.unlocked.displayName").replace("{level}",c+""));
                ArrayList<String> test = new ArrayList<>();
                test.add(ChatColor.GREEN + "Unlocked");
                test.add(" ");
                /*if(type == GUIHandle.UpgradeType.SPEED) test.add(ChatColor.GREEN + "Current " + WordUtils.capitalize(type.toString().toLowerCase()) + ": " + AttackDogs.getI().getWolfMap().get(p.getUniqueId()).getSpeed() * getCurrentLevel(p));
                if(type == GUIHandle.UpgradeType.DAMAGE) test.add(ChatColor.GREEN + "Current " + WordUtils.capitalize(type.toString().toLowerCase()) + ": " + AttackDogs.getI().getWolfMap().get(p.getUniqueId()).getDamage());
                if(type == GUIHandle.UpgradeType.REGEN) test.add(ChatColor.GREEN + "Current " + WordUtils.capitalize(type.toString().toLowerCase()) + ": " + AttackDogs.getI().getWolfMap().get(p.getUniqueId()).getRegen());
                */meta.setLore(test);
                unlocked.setItemMeta(meta);
                populate.setItem(slot,unlocked);
            }
            else if(c == i + 1){
                ItemMeta meta = next.getItemMeta();
                meta.setDisplayName(AttackDogs.getI().getF("gui.upgrade.next.displayName").replace("{level}", c + ""));
                ArrayList<String> test = new ArrayList<>();
                test.add(ChatColor.RED + "Cost: " + getCost(p,type));
                test.add(" ");
                test.add(ChatColor.AQUA + "+" + (type == GUIHandle.UpgradeType.REGEN ? (entry.getValue() - prev) : entry.getValue()*10) + " " + WordUtils.capitalize(type.toString().toLowerCase()));
                meta.setLore(test);
                next.setItemMeta(meta);
                populate.setItem(slot,next);
            }
            else{
                ItemMeta meta = locked.getItemMeta();
                meta.setDisplayName(AttackDogs.getI().getF("gui.upgrade.locked.displayName").replace("{level}",c+""));
                locked.setItemMeta(meta);
                populate.setItem(slot,locked);
            }
            prev = entry.getValue();
            c++;
            slot += 2;
        }
    }

    public int getCost(Player p, GUIHandle.UpgradeType type){
        int i = getCurrentLevel(p);
        int c = 1;
        for(Map.Entry<Integer,Double> entry : AttackDogs.getI().getUpgradeContainer(type).costStatMap.entrySet()){
            if(c == i) return entry.getKey();
            c++;
        }
        return -1;
    }


    public int getCurrentLevel(Player p){
            if(type == GUIHandle.UpgradeType.SPEED){
                return AttackDogs.getI().getWolfMap().get(p.getUniqueId()).getSl();
            }else if(type == GUIHandle.UpgradeType.DAMAGE){
                return AttackDogs.getI().getWolfMap().get(p.getUniqueId()).getDl();
            }else if(type == GUIHandle.UpgradeType.REGEN){
                return AttackDogs.getI().getWolfMap().get(p.getUniqueId()).getRl();
            }
        return -1;
    }

    public double getNextStat(Player p){
        int i = getCurrentLevel(p);
        int c = 1;
        for(Map.Entry<Integer,Double> entry : costStatMap.entrySet()){
            if(c == i+1){
                return entry.getValue();
            }
            c++;
        }
        return 0;
    }

    public ItemStack getNext() {
        return next;
    }

    public GUIHandle.UpgradeType getType() {
        return type;
    }
}
